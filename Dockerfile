FROM ubuntu:latest
MAINTAINER wailorman@gmail.com

RUN mkdir /var/www
ADD ./package.json /var/www/package.json
WORKDIR /var/www

RUN apt-get update && \
    apt-get install -y git git-core npm nodejs-legacy

RUN npm install

ADD . /var/www/

EXPOSE 1801
EXPOSE 27017

CMD [ "npm", "start" ]