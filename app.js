var restify       = require( 'restify' ),
    mongoose      = require( 'mongoose' ),
    invitesModule = require( './modules/invites.js' ),
    getDataModule = require( './modules/get-data.js' );

process.env.TZ = 'Europe/Moscow';

mongoose.connect( 'mongodb://mongo.local/ibb' );

var server = restify.createServer();

server.use( restify.queryParser() );
server.use( restify.bodyParser() );
server.use( restify.fullResponse() );

server.use( function ( req, res, next ) {
    res.header( "Access-Control-Allow-Origin", "*" );
    res.header( "Access-Control-Allow-Headers", "X-Requested-With" );
    res.charSet( 'utf-8' );
    return next();
} );

server.post( '/send-invite', invitesModule.sendInviteRoute );
server.post( '/person-became-a-member', invitesModule.makeMemberRoute );
server.get( '/is-already-invited', invitesModule.isAlreadyInvitedRoute );
server.get( '/get-data', getDataModule.getDataRoute );


// some change to test Dockerhub

server.listen( 1801, function () {
    console.log( 'Server started!' );
} );