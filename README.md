# IBB Analysis

All parameters (except parameters marked as **(required)**) is optional

## HTTP methods:

#### `POST /send-invite`
Use this method after you successfully send an invitation

+ **`invitedId`** -- profileId of person who invitation sent **(required)**
+ `city` -- person's city
+ `ageRange` -- age range passed to OK search filter
+ `inviterId` -- profileId of person who sent an invite

#### `POST /person-became-a-member`
Send this request on every members revision. It does not matter if you send this method with id of person who *already member*

+ `invitedId` -- profileId of person who invitation sent **(required)**

#### `GET /is-already-invited`
Use this method before send new invite. Maybe this person was already invited?

+ `invitedId` -- profileId of person you want to send invite **(required)**

#### `GET /get-data`
Returns data of last invited persons. The maximum distance between `from` and `to` is 30 days. If you pass only one parameter app will set distance to 10 days.
   **e.g.:** You want to see invites which sent before March 15. The oldest invite will dated March 5

+ `from` -- Date after you want to export invites data (in UNIX-time)
+ `to` -- Date before you want to export invites data (in UNIX-time)
