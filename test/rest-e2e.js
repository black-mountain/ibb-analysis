var should         = require( 'should' ),
    mongoose       = require( 'mongoose' ),
    restify        = require( 'restify' ),
    sugar          = require( 'sugar' ),
    async          = require( 'async' ),

    IbbMemberModel = require( '../modules/ibb-member-model.js' ),

    restifyClient  = restify.createJsonClient( {
        url:     'http://localhost:1801/',
        version: '*'
    } );

var cleanUp = function ( next ) {

    //var idsToRemove = [ 5452259465, 84540220202, 845402202020, 4004040404, "30303030" ],
    //    orStatement = [];
    //
    //idsToRemove.forEach( function ( id ) {
    //
    //    orStatement.push( { invitedId: id } );
    //
    //} );

    IbbMemberModel.find().remove().exec( next );

};

var generateDataForGetDataMethod = function ( done ) {

    async.timesSeries( 31, function ( n, tcb ) {

        var nDay = n + 1;

        var newInvite = new IbbMemberModel( {
            invitedId:   nDay,
            invitedDate: new Date( '2015-03-' + nDay )
        } );

        newInvite.save( function ( err, doc ) {

            should.not.exist( err );
            tcb();

        } );

    }, done );

};

describe( 'Clients module e2e', function () {

    before( function ( done ) {

        mongoose.connect(
            'mongodb://mongo.local/ibb', {},
            function ( err ) {
                should.not.exist( err );
                done();
            }
        );

    } );

    before( function ( done ) {

        cleanUp( done );

    } );

    after( function ( done ) {

        cleanUp( done );

    } );

    var tpls = {

        /**
         *
         * @param params
         * @param doesShouldReturnError
         * @param next
         */
        sendInvite: function ( params, doesShouldReturnError, next ) {

            // city, invitedId, ageRange, inviterId

            var httpRequestResult,
                curDate = new Date();

            async.series(
                [

                    // . send http request
                    function ( scb ) {

                        restifyClient.post(
                            '/send-invite',
                            params,
                            function ( err, req, res, data ) {

                                if ( doesShouldReturnError ) {
                                    should.exist( err );
                                } else {
                                    should.not.exist( err );
                                }

                                scb( err );

                            }
                        );

                    },

                    // . check in DB
                    function ( scb ) {

                        IbbMemberModel.findOne(
                            { invitedId: params.invitedId },
                            function ( err, doc ) {

                                should.not.exist( err );

                                Object.each( params, function ( paramName, value ) {

                                    doc[ paramName ].should.eql( value );

                                } );

                                doc.invitedDate.getHours().should.eql( curDate.getHours() );

                                scb();

                            }
                        );

                    }

                ],
                function () {
                    next();
                }
            );


        },

        /**
         *
         * @param invitedId
         * @param exceptedResult
         * @param next ()
         */
        isAlreadyInvited: function ( invitedId, exceptedResult, next ) {

            restifyClient.get( '/is-already-invited?invitedId=' + invitedId, function ( err, req, res, data ) {

                if ( exceptedResult === 'error' ) {
                    should.exist( err );
                } else {
                    should.not.exist( err );
                    data.result.should.eql( exceptedResult );
                }

                next();

            } );

        },

        /**
         *
         * @param invitedId
         * @param doesShouldReturnError
         * @param next (result)
         */
        becameMember: function ( invitedId, doesShouldReturnError, next ) {

            restifyClient.post( '/person-became-a-member?invitedId=' + invitedId, function ( err, req, res, data ) {

                if ( doesShouldReturnError ) {
                    should.exist( err );
                } else {
                    should.not.exist( err );
                }

                next( data );

            } );

        },

        /**
         *
         * @param {object} [filter]
         * @param {number} [filter.from]
         * @param {number} [filter.to]
         * @param {Array} expectedRange
         * @param {boolean} doesShouldReturnError
         * @param done
         */
        getData: function ( filter, expectedRange, doesShouldReturnError, done ) {

            if ( filter.from && filter.from ) filter.from = ( new Date( '2015-03-' + filter.from ) ).getTime();
            if ( filter.to && filter.to ) filter.to = ( new Date( '2015-03-' + filter.to ) ).getTime();


            var url;
            url = '/get-data';
            if ( filter.from ) url += '?from=' + filter.from + '&';
            if ( filter.to && !filter.from ) url += '?to=' + filter.to;
            if ( filter.to ) url += 'to=' + filter.to;

            restifyClient.get( url, function ( err, req, res, data ) {

                if ( doesShouldReturnError ) {
                    should.exist( err );
                    done();
                } else {
                    should.not.exist( err );
                }

                // check results
                if ( expectedRange[ 1 ] )
                    ( new Date( data[ 0 ].invitedDate ).getDate() ).should.eql( expectedRange[ 1 ] );

                if ( expectedRange[ 0 ] )
                    ( new Date( data[ data.length - 1 ].invitedDate ).getDate() ).should.eql( expectedRange[ 0 ] );

                done();

            } );

        }

    };

    describe( 'POST /send-invite', function () {

        it( 'should write invitation with all fields', function ( done ) {

            // city, invitedId, ageRange, inviterId

            tpls.sendInvite(
                {
                    city:      'Москва',
                    invitedId: "5452259465",
                    ageRange:  '50..90',
                    inviterId: "73882728"
                },
                false,
                done
            );

        } );

        it( 'should write invitation with only invitedId', function ( done ) {

            tpls.sendInvite(
                {
                    invitedId: "5452259465"
                },
                false,
                done
            );

        } );

        it( 'should not write invitation with no params', function ( done ) {

            tpls.sendInvite(
                {},
                true,
                done
            );

        } );

        it( 'should not write second document on second invitation', function ( done ) {

            tpls.sendInvite(
                {
                    invitedId: "5452259465"
                },
                false,
                function () {

                    IbbMemberModel.find( { invitedId: "5452259465" }, function ( err, docs ) {

                        should.not.exist( err );

                        docs.should.have.property( 'length' );
                        docs.length.should.eql( 1 );

                        done();

                    } );

                }
            );

        } );

    } );

    describe( 'POST /person-became-a-member', function () {

        it( 'should return 404 when try to became a member not invited person', function ( done ) {

            tpls.becameMember(
                845402202020,
                true,
                function () {
                    //result.should.eql({});
                    done();
                }
            );

        } );

        it( 'should mark as "isMember" and set timestamp', function ( done ) {

            var curDate = new Date();

            tpls.becameMember(
                "5452259465",
                false,
                function ( result ) {

                    result.should.eql( { result: 'successful' } );

                    IbbMemberModel.findOne( { invitedId: "5452259465" }, function ( err, doc ) {

                        should.not.exist( err );

                        doc.isMember.should.eql( true );

                        ( doc.invitedDate.getHours() ).should.eql( curDate.getHours() );

                        done();

                    } );

                }
            );

        } );

        it( 'should return "already member" when try to became a member person who already member', function ( done ) {

            tpls.becameMember(
                "5452259465",
                false,
                function ( result ) {
                    result.should.eql( { result: 'already member' } );

                    done();
                }
            );

        } );

    } );

    describe( 'GET /is-already-invited', function () {

        it( 'should return error on empty invitedId', function ( done ) {

            tpls.isAlreadyInvited(
                null,
                'error',
                done
            );

        } );

        it( 'should return true for already invited person', function ( done ) {

            tpls.isAlreadyInvited(
                "5452259465",
                true,
                done
            );

        } );

        it( 'should return true for person who didnt became a member', function ( done ) {

            async.series( [

                // . send invite
                function ( scb ) {

                    tpls.sendInvite(
                        {
                            invitedId: "4004040404"
                        },
                        false,
                        scb
                    );

                },

                // . check membership
                function ( scb ) {

                    tpls.isAlreadyInvited(
                        "4004040404",
                        true,
                        scb
                    );

                }

            ], done );

        } );

        it( 'should return false for person who was not invited', function ( done ) {

            tpls.isAlreadyInvited(
                "30303030",
                false,
                done
            );

        } );


    } );

    describe( 'GET /get-data', function () {

        before( function ( done ) {
            cleanUp( done );
        } );

        before( function ( done ) {
            generateDataForGetDataMethod( done );
        } );

        it( 'B', function ( done ) {

            // 86400000

            async.series( [

                // generate invite
                function ( scb ) {

                    tpls.sendInvite(
                        {
                            invitedId: '1234'
                        },
                        false,
                        scb
                    );

                },

                // find invite
                function ( scb ) {

                    tpls.getData(
                        {},
                        [ (new Date()).getDate(), null ],
                        false,
                        scb
                    );

                }

            ], done );

        } );

        it( 'C', function ( done ) {

            tpls.getData(
                {
                    from: 2,
                    to:   31
                },
                [ 2, 31 ],
                false,
                done
            );

        } );

        it( 'D', function ( done ) {

            tpls.getData(
                {
                    from: 1,
                    to:   10
                },
                [ 1, 10 ],
                false,
                done
            );

        } );

        it( 'E', function ( done ) {

            tpls.getData(
                {
                    from: 1,
                    to:   31
                },
                [ 1, 31 ],
                true,
                done
            );

        } );

        it( 'F', function ( done ) {

            tpls.getData(
                {
                    to: 26
                },
                [ 17, 26 ],
                false,
                done
            );

        } );

        it( 'G', function ( done ) {

            tpls.getData(
                {
                    from: 5
                },
                [ 5, 14 ],
                false,
                done
            );

        } );

    } );


} );