var mongoose       = require( 'mongoose' ),
    restify        = require( 'restify' ),
    sugar          = require( 'sugar' ),
    async          = require( 'async' ),
    IbbMemberModel = require( './ibb-member-model.js' );


// /send-invite
// /person-became-a-member
// /is-already-member


var validators = {
    id: function ( val, idType ) {
        if ( val && typeof val !== 'string' && typeof val !== 'number' ) {

            switch ( idType ) {
                case 'invited':
                    throw new restify.InvalidArgumentError( 'invitedId is invalid' );
                    break;
                case 'inviter':
                    throw new restify.InvalidArgumentError( 'inviterId is invalid' );
                    break;
                default:
                    throw new restify.InvalidArgumentError( 'some id is invalid' );
                    break;
            }
        }
    },

    city: function ( val ) {

        if ( val && typeof val !== 'string' ) {
            throw new restify.InvalidArgumentError( 'city is invalid' );
        }

    },

    ageRange: function ( val ) {

        if ( val && typeof val !== 'string' ) {
            throw new restify.InvalidArgumentError( 'ageRange is invalid' );
        }

    }
};

/**
 *
 * @param parameters
 * @param parameters.invitedId
 * @param parameters.city
 * @param parameters.ageRange
 * @param parameters.inviterId
 * @param next (err)
 */
var sendInvite = function ( parameters, next ) {

    if ( !parameters.invitedId ) return next( new restify.InvalidArgumentError( 'invitedId missing' ) );

    //if ( typeof parameters.invitedId === 'string' ) parameters.invitedId = parseInt( parameters.invitedId );

    //try {
    //
    //    validators.id( parameters.invitedId, 'invited' );
    //    validators.id( parameters.inviterId, 'inviter' );
    //    validators.city( parameters.city );
    //    validators.ageRange( parameters.ageRange );
    //
    //} catch (e) {
    //    return next( e );
    //}

    var newDocumentData = {};

    Object.each( parameters, function ( key, value ) {

        newDocumentData[ key ] = value;

    } );

    newDocumentData.invitedDate = new Date();

    var newIbbMember = new IbbMemberModel( newDocumentData );


    // check existent
    IbbMemberModel.findOne( { invitedId: parameters.invitedId }, function ( err, doc ) {

        if ( err ) return next( new restify.InternalError( 'Mongo check existent: ' + err.message ) );

        if ( doc ) return next();  // if member with such invitedId exist -> exit

        newIbbMember.save( function ( err ) {

            if ( err ) return next( new restify.InternalError( 'Mongo write: ' + err.message ) );

            next();

        } );

    } );

};

var sendInviteRoute = function ( req, res, next ) {

    var parametersFieldNames = [ 'city', 'invitedId', 'ageRange', 'inviterId' ],
        inviteParameters = {};

    parametersFieldNames.forEach( function ( fieldName ) {

        inviteParameters[ fieldName ] = req.params[ fieldName ];

    } );

    sendInvite( inviteParameters, function ( err ) {

        if ( err ) return next( err );

        res.send( 200, 'Member was wrote to DB' );
        return next();

    } );

};


var makeMember = function ( invitedId, next ) {

    if ( !invitedId ) return next();

    if ( typeof invitedId === 'string' ) invitedId = parseInt( invitedId );

    //try {
    //    validators.id( invitedId, 'invited' );
    //} catch (e) {
    //    return next();
    //}

    IbbMemberModel.findOne( { invitedId: invitedId }, function ( err, doc ) {

        if ( err ) return next( new restify.InternalError( 'Mongo find: ' + err.message ) );

        if ( !doc ) return next( new restify.ResourceNotFoundError( 'Cant find invite with such id' ) );

        if ( doc.isMember ) {
            return next( new Error( 'already member' ) );
        }

        doc.isMember = true;
        doc.becameMemberDate = new Date();

        doc.save( function ( err ) {

            if ( err ) return next( new restify.InternalError( 'Mongo write: ' + err.message ) );
            return next();

        } );

    } );

};

var makeMemberRoute = function ( req, res, next ) {

    makeMember( req.params.invitedId, function ( err ) {

        if ( err && err instanceof Error && err.message === 'already member' ) {

            res.send( 200, { result: 'already member' } );
            return next();

        } else if ( err ) {

            return next( err );

        } else {

            res.send( 200, { result: 'successful' } );
            next();

        }

    } );

};

var isAlreadyInvited = function ( invitedId, next ) {

    if ( !invitedId || invitedId === 'null' ) return next( new restify.InvalidArgumentError( '"invitedId" parameter missing' ) );

    if ( typeof invitedId === 'string' ) invitedId = parseInt( invitedId );

    IbbMemberModel.findOne( { invitedId: invitedId }, function ( err, doc ) {

        if ( err ) return next( new restify.InternalError( 'Mongo find: ' + err.message ) );

        if ( !doc ) return next( null, false );

        return next( null, true );

    } );

};

var isAlreadyInvitedRoute = function ( req, res, next ) {

    isAlreadyInvited( req.params.invitedId, function ( err, result ) {

        if ( err ) return next( err );

        if ( result ) {
            res.send( 200, { result: true } );
        } else {
            res.send( 200, { result: false } );
        }

        next();

    } );

};

module.exports.sendInviteRoute = sendInviteRoute;
module.exports.makeMemberRoute = makeMemberRoute;
module.exports.isAlreadyInvitedRoute = isAlreadyInvitedRoute;