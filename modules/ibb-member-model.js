var mongoose = require( 'mongoose' ),
    Schema   = mongoose.Schema;

module.exports = mongoose.model( 'IbbMemberModel', new Schema( {

    invitedId: {
        type: String,
        required: true
    },

    city: {
        type: String
    },

    ageRange: {
        type: String
    },

    inviterId: {
        type: String
    },

    invitedDate: {
        type: Date
    },

    isMember: {
        type: Boolean
    },

    becameMemberDate: {
        type: Date
    },

    lastMembershipChecked: {
        type: Date
    }

}, {

    collection: 'members'

} ) );
