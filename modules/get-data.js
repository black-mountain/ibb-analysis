var mongoose       = require( 'mongoose' ),
    restify        = require( 'restify' ),
    sugar          = require( 'sugar' ),
    async          = require( 'async' ),
    IbbMemberModel = require( './ibb-member-model.js' );

/**
 * Get invites data
 *
 * @param {object} filter
 * @param {number} filter.from
 * @param {number} filter.to
 * @param next
 */
var getData = function ( filter, next ) {
    var millisecondsInADay = 86400000,
        curDate = new Date(),
        range = {};

    // Getting data from arguments
    if ( filter.from && !isNaN( parseInt( filter.from ) ) ) {
        range.from = new Date( parseInt( filter.from ) );
    }

    if ( filter.to && !isNaN( parseInt( filter.to ) ) ) {
        range.to = new Date( parseInt( filter.to ) );
    }


    // Checking for empty fields & if somebody if empty -> set default value
    if ( range.from && !range.to ) {
        range.to = new Date( range.from.getTime() + millisecondsInADay * 10 );
    } else if ( !range.from && range.to ) {
        range.to.setHours(23);
        range.to.setMinutes(59);
        range.to.setSeconds(59);
        range.from = new Date( range.to.getTime() - millisecondsInADay * 10 );
    } else if ( !range.from && !range.to ) {
        range = { from: new Date( curDate.getTime() - millisecondsInADay * 10 ), to: curDate };
    }

    // checking range
    var rangeLength = range.to.getTime() - range.from.getTime();

    if ( rangeLength > millisecondsInADay * 30 ) {
        return next( new restify.InternalError( 'Date range can not be greater than 30 days' ) );
    }

    // prepare query
    var query = {
        invitedDate: {
            "$lte": range.to,
            "$gte": range.from
        }
    };

    IbbMemberModel
        .find( query )
        .sort( { invitedDate: -1 } )
        .exec( function ( err, docs ) {

            if ( err ) return next( new restify.InternalError( 'Mongo find: ' + err.message ) );

            next( null, docs );

        } );

};

var getDataRoute = function ( req, res, next ) {

    getData( { from: req.params.from, to: req.params.to }, function ( err, data ) {

        if ( err ) return next( err );

        res.send( 200, data );

        return next();

    } );

};


module.exports.getDataRoute = getDataRoute;